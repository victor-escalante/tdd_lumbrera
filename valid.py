import re


class ValidRequest:

    list_errors = []
    request = {}
    valid_form = True

    def __init__(self, request, valid_request):
        self.request = request
        self.valid_request = valid_request

    def validator(self, value: str, validations: str):
        self.list_errors = []
        split_validations = {}

        separate_validations = validations.split(";")
        for x in separate_validations:
            x = x.split("=")
            split_validations[x[0]] = x[1]

        if (value not in self.request) and ("required" in split_validations):
            self.valid_form = False
            self.list_errors.append("se esperaba el valor de " + value)
            return

        if "is_string" in split_validations:
            if not isinstance(self.request[value], str):
                self.valid_form = False
                self.list_errors.append("se esperaba un valor string para " + value)
                return

        if "is_int" in split_validations:
            if not isinstance(self.request[value], int):
                self.list_errors.append("se esperaba un valor entero para " + value)
                self.valid_form = False
                return

        if "min" in split_validations:
            if len(self.request[value]) < int(split_validations["min"]):
                self.valid_form = False
                self.list_errors.append(
                    "se esperaba que el valor fuera mayor a " + split_validations["min"]
                )
                return

        if "max" in split_validations:
            if len(self.request[value]) > int(split_validations["max"]):
                self.valid_form = False
                self.list_errors.append(
                    "se esperaba que el valor fuera menor a " + split_validations["max"]
                )
                return

    def is_valid(self):
        return self.valid_form

    def errors(self):
        return self.list_errors

    def validate(self):
        list_errors = []
        response = True

        for attr in self.valid_request:
            self.validator(attr, self.valid_request[attr])
            if not self.is_valid():
                list_errors.append(self.errors())

        if list_errors:
            response = False

        return response


def is_url_valid(url: str):

    regex = re.compile(
        r"^(?:http|ftp)s?://"
        r"(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|"
        r"localhost|"
        r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})"
        r"(?::\d+)?"
        r"(?:/?|[/?]\S+)$",
        re.IGNORECASE,
    )

    if re.match(regex, url) is None:
        return False

    if 0 >= len(url) >= 100:
        return False

    return True
