import unittest
import mock
import login


class TddLogin(unittest.TestCase):
    @mock.patch("login.get_user")
    def test_1_success(self, mock_fn):
        item = {"id": 1, "username": "Juan"}
        url = "https://www.google.com"
        mock_fn.return_value = {
            "id": item["id"],
            "username": item["username"],
            "is_active": True,
            "grants": [url],
        }
        self.assertTrue(login.run(item, url))

    @mock.patch("login.get_user")
    def test_2_check_value_min_username(self, mock_fn):
        item = {"id": 1, "username": "M"}
        url = "www.google.com"
        mock_fn.return_value = {
            "id": item["id"],
            "username": item["username"],
            "is_active": True,
            "grants": [url],
        }
        self.assertFalse(login.run(item, url))

    @mock.patch("login.get_user")
    def test_3_check_value_max_username(self, mock_fn):
        item = {"id": 1, "username": "MaaximiumMaxiumMaxium"}
        url = "www.google.com"
        mock_fn.return_value = {
            "id": item["id"],
            "username": item["username"],
            "is_active": True,
            "grants": [url],
        }
        self.assertFalse(login.run(item, url))

    @mock.patch("login.get_user")
    def test_4_not_send_id(self, mock_fn):
        item = {"username": "MaaximiumMaxiumMaxium"}
        url = "www.google.com"
        mock_fn.return_value = {
            "username": item["username"],
            "is_active": True,
            "grants": [url],
        }
        self.assertFalse(login.run(item, url))

    @mock.patch("login.get_user")
    def test_5_not_send_data(self, mock_fn):
        item = {}
        url = "www.google.com"
        mock_fn.return_value = {"is_active": True, "grants": [url]}
        self.assertFalse(login.run(item, url))

    @mock.patch("login.get_user")
    def test_6_with_correct_url(self, mock_fn):
        item = {"id": 1, "username": "Juan"}
        url = "https://www.google.com"
        mock_fn.return_value = {
            "id": item["id"],
            "username": item["username"],
            "is_active": True,
            "grants": [url],
        }
        self.assertTrue(login.run(item, url))

    @mock.patch("login.get_user")
    def test_7_with_incorrect_url(self, mock_fn):
        item = {"id": 1, "username": "Juan"}
        url = "https://www.googler.com"
        mock_fn.return_value = {
            "id": item["id"],
            "username": item["username"],
            "is_active": True,
            "grants": ["https://www.google.com"],
        }
        self.assertFalse(login.run(item, url))

    @mock.patch("login.get_user")
    def test_8_not_grants(self, mock_fn):
        item = {"id": 1, "username": "Juan"}
        url = "https://www.googler.com"
        mock_fn.return_value = {
            "id": item["id"],
            "username": item["username"],
            "is_active": True,
            "grants": [],
        }
        self.assertFalse(login.run(item, url))

    @mock.patch("login.get_user")
    def test_9_check_url_not_dot_com(self, mock_fn):
        item = {"id": 1, "username": "Juan"}
        url = "https://www.googlercom"
        mock_fn.return_value = {
            "id": item["id"],
            "username": item["username"],
            "is_active": True,
            "grants": ["https://www.googler.com"],
        }
        self.assertFalse(login.run(item, url))

    @mock.patch("login.get_user")
    def test_10_check_url_with_protocol(self, mock_fn):
        item = {"id": 1, "username": "Juan"}
        url = "https://www.googlercom:3000"
        mock_fn.return_value = {
            "id": item["id"],
            "username": item["username"],
            "is_active": True,
            "grants": [url, "https://www.googler.com"],
        }
        self.assertTrue(login.run(item, url))

    @mock.patch("login.get_user")
    def test_11_check_url_localhost(self, mock_fn):
        item = {"id": 1, "username": "Juan"}
        url = "https://localhost"
        mock_fn.return_value = {
            "id": item["id"],
            "username": item["username"],
            "is_active": True,
            "grants": ["https://localhost", "https:www.google.com"],
        }
        self.assertTrue(login.run(item, url))

    @mock.patch("login.get_user")
    def test_12_check_url_ip(self, mock_fn):
        item = {"id": 1, "username": "Juan"}
        url = "http://127.0.0.1"
        mock_fn.return_value = {
            "id": item["id"],
            "username": item["username"],
            "is_active": True,
            "grants": [url, "https:www.google.com"],
        }
        self.assertTrue(login.run(item, url))

    @mock.patch("login.get_user")
    def test_13_check_url_none_format(self, mock_fn):
        item = {"id": 1, "username": "Juan"}
        url = "1"
        mock_fn.return_value = {
            "id": item["id"],
            "username": item["username"],
            "is_active": True,
            "grants": [url, "https:www.google.com"],
        }
        self.assertFalse(login.run(item, url))

    @mock.patch("login.get_user")
    def test_14_check_url_username_int(self, mock_fn):
        item = {"id": 1, "username": 1}
        url = "1"
        mock_fn.return_value = {
            "id": item["id"],
            "username": item["username"],
            "is_active": True,
            "grants": [url, "https:www.google.com"],
        }
        self.assertFalse(login.run(item, url))

    @mock.patch("login.get_user")
    def test_15_check_url_username_int(self, mock_fn):
        item = {"id": 1, "username": 1}
        url = "1"
        mock_fn.return_value = {
            "id": item["id"],
            "username": item["username"],
            "is_active": True,
            "grants": [url, "https:www.google.com"],
        }
        self.assertFalse(login.run(item, url))

    @mock.patch("login.get_user")
    def test_16_check_id_string(self, mock_fn):
        item = {"id": "djdjdjd", "username": "juan"}
        url = "1"
        mock_fn.return_value = {
            "id": item["id"],
            "username": item["username"],
            "is_active": True,
            "grants": [url, "https:www.google.com"],
        }
        self.assertFalse(login.run(item, url))
