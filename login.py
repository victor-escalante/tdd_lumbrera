from typing import List
from db import get_user, is_url_in_grants
from valid import ValidRequest, is_url_valid


def run(item: object, url: str):

    list_errors = []

    valid_rules = {
        "id": "required=True;is_int=True",
        "username": "required=True;min=3;max=10;is_string=True",
    }

    validate_request = ValidRequest(item, valid_rules)
    validate_url = is_url_valid(url)

    result = validate_request.validate()

    if result is False:
        list_errors.append(validate_request.errors())
        return False

    if validate_url is False:
        list_errors.append("Formato de url invalido")
        return False

    user = get_user()

    if is_url_in_grants(url, user["grants"]):
        return True
    return False
